export class Pagination<T> {
  limit: number;
  offset: number;
  orderby: OrderByType;
  order: T;
}

enum OrderByType {
  DESC = 'DESC',
  ASC = 'ASC',
}
