import { Author } from './entities/author.entity';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { CreateAuthorDto } from './dto/create-author.dto';
import { UpdateAuthorDto } from './dto/update-author.dto';
import { TypeOrmQueryService } from '@nestjs-query/query-typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { InjectMapper } from '@automapper/nestjs';
import { isEmpty, Mapper } from '@automapper/core';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthorsService extends TypeOrmQueryService<Author> {
  constructor(
    @InjectRepository(Author) private authorRepository: Repository<Author>,
    @InjectMapper() private readonly classMapper: Mapper,
  ) {
    super(authorRepository, { useSoftDelete: true });
  }

  async create(createAuthorDto: CreateAuthorDto): Promise<Author> {
    try {
      const entity = this.classMapper.map(
        createAuthorDto,
        CreateAuthorDto,
        Author,
      );
      return this.authorRepository.create(entity);
    } catch (ex) {
      throw new Error(`create author error: ${ex.message}.`);
    }
  }

  async findAll(): Promise<[Author[], number]> {
    try {
      return await this.authorRepository.findAndCount();
    } catch (ex) {
      throw new Error(`findAll author error: ${ex.message}.`);
    }
  }

  async findOne(id: string): Promise<Author | null> {
    const author = await this.authorRepository.findOneBy({ id });
    if (isEmpty(author))
      throw new HttpException(
        `not found author id ${id}`,
        HttpStatus.NOT_FOUND,
      );

    return author;
  }

  async getByEmail(email: string): Promise<Author> {
    const author = await this.authorRepository.findOneBy({ email });
    if (isEmpty(author))
      throw new HttpException(
        `not found author email ${email}`,
        HttpStatus.NOT_FOUND,
      );

    return author;
  }

  async update(id: string, updateBlogDto: UpdateAuthorDto): Promise<Author> {
    const entity = this.classMapper.map(updateBlogDto, UpdateAuthorDto, Author);
    entity.id = id;
    try {
      return await this.authorRepository.save(entity);
    } catch (ex) {
      throw new HttpException(
        `update author error: ${ex.message}.`,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async remove(id: string): Promise<void> {
    if (!id) throw new Error(`remove error: id is empty.`);
    try {
      const blog = await this.findOne(id);
      this.authorRepository.softRemove(blog);
    } catch (ex) {
      throw new HttpException(
        `remove author error: ${ex.message}.`,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async getUserIfRefreshTokenMatches(refreshToken: string, authorId: string) {
    const author = await this.getById(authorId);

    const isRefreshTokenMatching = await bcrypt.compare(
      refreshToken,
      author.currentHashedRefreshToken,
    );

    if (isRefreshTokenMatching) {
      return author;
    }
  }

  async setCurrentRefreshToken(refreshToken: string, authorId: string) {
    const currentHashedRefreshToken = await bcrypt.hash(refreshToken, 10);
    await this.authorRepository.update(authorId, {
      currentHashedRefreshToken,
    });
  }

  async removeRefreshToken(authorId: string): Promise<Author> {
    this.authorRepository.update(authorId, {
      currentHashedRefreshToken: null,
    });
    return this.findOne(authorId);
  }

  async createWithGoogle(email: string, name: string): Promise<Author> {
    const newUser = this.authorRepository.create({
      email,
      fullName: name,
    });
    await this.authorRepository.save(newUser);
    return newUser;
  }
}
