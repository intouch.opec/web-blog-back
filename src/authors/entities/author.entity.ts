import { Entity, Column, ManyToMany, JoinTable } from 'typeorm';
import { BaseEntity } from '../../core/base.entity';
import { Exclude } from 'class-transformer';
import { Blog } from '../../blogs/entities/blog.entity';

@Entity({ name: 'authors' })
export class Author extends BaseEntity {
  @Column({ type: 'text' })
  fullName: string;

  @Column({ type: 'text', nullable: true })
  avatar: string;

  @Column({ type: 'text', nullable: true })
  occupation: string;

  @Column({ type: 'text', nullable: true })
  company: string;

  @Column({ type: 'text' })
  email: string;

  @Column({ type: 'text', nullable: true })
  twitter: string;

  @Column({ type: 'text', nullable: true })
  linkedin: string;

  @Column({ type: 'text', nullable: true })
  github: string;

  @Column({ type: 'text', nullable: true })
  layout: string;

  @Column({
    nullable: true,
  })
  @Exclude()
  public currentHashedRefreshToken?: string;

  @Column({ nullable: true })
  @Exclude()
  public password?: string;

  @ManyToMany(() => Blog)
  @JoinTable()
  authors: Blog[];
}
