import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { CreateBlogDto } from './dto/create-blog.dto';
import { UpdateBlogDto } from './dto/update-blog.dto';
import { Blog } from './entities/blog.entity';
import { TypeOrmQueryService } from '@nestjs-query/query-typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { InjectMapper } from '@automapper/nestjs';
import { Mapper } from '@automapper/core';
import { AuthorsService } from '../authors/authors.service';

@Injectable()
export class BlogsService extends TypeOrmQueryService<Blog> {
  constructor(
    @InjectRepository(Blog) private blogRepository: Repository<Blog>,
    private readonly authorsService: AuthorsService,
    @InjectMapper() private readonly classMapper: Mapper,
  ) {
    super(blogRepository, { useSoftDelete: true });
  }

  async create(createBlogDto: CreateBlogDto): Promise<Blog> {
    try {
      const entity = this.classMapper.map(createBlogDto, CreateBlogDto, Blog);
      return this.blogRepository.create(entity);
    } catch (ex) {
      throw new HttpException(
        `create blog error ${ex.message}}`,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async findAll(): Promise<[Blog[], number]> {
    return await this.blogRepository.findAndCount();
  }

  async findOne(id: string): Promise<Blog> {
    if (!id)
      throw new HttpException(
        `remove blog error: id is empty.`,
        HttpStatus.BAD_REQUEST,
      );
    const blog = await this.blogRepository.findOneBy({ id });
    if (!blog)
      throw new HttpException(
        `Error during remove, item not found => id: ${id}}`,
        HttpStatus.NOT_FOUND,
      );
    return blog;
  }

  async update(id: string, updateBlogDto: UpdateBlogDto): Promise<Blog> {
    const entity = this.classMapper.map(updateBlogDto, UpdateBlogDto, Blog);
    entity.id = id;
    try {
      return await this.blogRepository.save(entity);
    } catch (ex) {
      throw new Error(`update error: ${ex.message}.`);
    }
  }

  async remove(id: string): Promise<void> {
    if (!id)
      throw new HttpException(
        `remove blog error: id is empty.`,
        HttpStatus.BAD_REQUEST,
      );
    try {
      const blog = await this.findOne(id);

      this.blogRepository.softRemove(blog);
    } catch (ex) {
      throw new HttpException(
        `remove error: ${ex.message}.`,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
  async addOwnerAuthor(id: string, authorId: string) {
    const blog = await this.findOne(id);
    const author = await this.authorsService.findById(authorId);
    blog.authors.push(author);
    this.blogRepository.save(blog);
  }

  async verifyOwner(ownerId: string, blog: Blog) {
    const match = blog.authors.find((author) => author.id === ownerId);
    if (!match)
      throw new HttpException(
        `The Owner Id ${ownerId} is not owner blog ${blog.id}`,
        HttpStatus.BAD_REQUEST,
      );
  }
}
