import { Author } from '../../authors/entities/author.entity';
import { Entity, Column, ManyToMany, JoinTable } from 'typeorm';
import { BaseEntity } from '../../core/base.entity';

@Entity({ name: 'blog' })
export class Blog extends BaseEntity {
  @Column({ type: 'varchar', length: 300 })
  title: string;

  @Column({ type: 'text' })
  description: string;

  @Column({ type: 'timestamptz' })
  date: Date;

  @Column({ type: 'json', array: true, default: [] })
  tags: string[];

  @Column({ type: 'boolean' })
  draft: boolean;

  @Column({ type: 'varchar', length: 300 })
  summary: string;

  @Column({ type: 'json', array: true, default: [] })
  images: string[];

  @ManyToMany(() => Author)
  @JoinTable()
  authors: Author[];

  @Column({ type: 'varchar', length: 300 })
  layout: string;

  @Column({ type: 'varchar', length: 300 })
  bibliography: string;

  @Column({ type: 'varchar', length: 300 })
  canonicalUrl: string;
}
