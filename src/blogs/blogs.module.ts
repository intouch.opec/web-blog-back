import { Module } from '@nestjs/common';
import { BlogsService } from './blogs.service';
import { BlogsController } from './blogs.controller';
import { BlogProfile } from './blogs.mapper';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Blog } from './entities/blog.entity';
import { AuthorsModule } from '../authors/authors.module';

@Module({
  imports: [TypeOrmModule.forFeature([Blog]), AuthorsModule],
  controllers: [BlogsController],
  providers: [BlogProfile, BlogsService],
})
export class BlogsModule {}
