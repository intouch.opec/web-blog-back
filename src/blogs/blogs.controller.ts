import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  UseGuards,
  Post,
  Headers,
  HttpException,
  HttpStatus,
  UsePipes,
  ValidationPipe,
  Put,
} from '@nestjs/common';
import { BlogsService } from './blogs.service';
import { CreateBlogDto } from './dto/create-blog.dto';
import { UpdateBlogDto } from './dto/update-blog.dto';
import { AddAuthorBlogDto } from './dto/add-author-blog.dto';
import { Blog } from './entities/blog.entity';
import { ApiTags } from '@nestjs/swagger';
import JwtAuthenticationGuard from '../auth/jwt-authentication.guard';

@ApiTags('Blog')
@Controller('blogs')
export class BlogsController {
  constructor(private readonly blogsService: BlogsService) {}

  @Post()
  @UseGuards(JwtAuthenticationGuard)
  @UsePipes(new ValidationPipe({ transform: true }))
  async create(@Body() blogDto: CreateBlogDto) {
    await this.blogsService.create(blogDto);
  }

  @Get()
  async findAll(): Promise<[Blog[], number]> {
    return this.blogsService.findAll();
  }

  @Put('/:id')
  @UsePipes(new ValidationPipe({ transform: true }))
  @UseGuards(JwtAuthenticationGuard)
  async update(
    @Body() updateBlogDto: UpdateBlogDto,
    @Param('id') id: string,
    // @Headers(JWT_HEADER_PARAM) token,
  ) {
    return await this.blogsService.update(id, updateBlogDto);
  }

  @Delete('/:id')
  @UseGuards(JwtAuthenticationGuard)
  async delete(@Param('id') id: string) {
    await this.blogsService.remove(id);
  }

  @Post('/:id/authors')
  @UseGuards(JwtAuthenticationGuard)
  async addOwnerBlog(
    @Body() addAuthorBlogDto: AddAuthorBlogDto,
    @Param('id') id: string,
  ) {
    await this.blogsService.addOwnerAuthor(id, addAuthorBlogDto.authorId);
  }
}
