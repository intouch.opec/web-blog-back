import { AutoMap } from '@automapper/classes';
import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateBlogDto {
  @AutoMap()
  @IsNotEmpty()
  @ApiProperty()
  title: string;

  @AutoMap()
  @IsNotEmpty()
  @ApiProperty()
  description: string;

  @AutoMap()
  @ApiProperty()
  tags: string[];

  @AutoMap()
  @ApiProperty()
  draft: boolean;

  @AutoMap()
  @IsNotEmpty()
  @ApiProperty()
  images: string[];

  @AutoMap()
  @IsNotEmpty()
  @ApiProperty()
  authors: string[];

  @AutoMap()
  @ApiProperty()
  canonicalUrl: string;
}
