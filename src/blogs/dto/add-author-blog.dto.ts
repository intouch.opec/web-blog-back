import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class AddAuthorBlogDto {
  @IsNotEmpty()
  @ApiProperty()
  authorId: string;
}
