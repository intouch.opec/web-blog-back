import { Request } from 'express';
import { Author } from '../authors/entities/author.entity';

interface RequestWithUser extends Request {
  author: Author;
}

export default RequestWithUser;
