interface TokenPayload {
  userId: string;
  isSecondFactorAuthenticated?: boolean;
  fullName: string;
}

export default TokenPayload;
