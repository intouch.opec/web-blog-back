import {
  Body,
  Req,
  Controller,
  HttpCode,
  Post,
  UseGuards,
  Get,
  ClassSerializerInterceptor,
  UseInterceptors,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import RequestWithUser from './request-with-author.interface';
import { LocalAuthenticationGuard } from './local-authentication.guard';
import JwtAuthenticationGuard from './jwt-authentication.guard';
import { AuthorsService } from '../authors/authors.service';
import JwtRefreshGuard from './jwt-refresh.guard';
import { ApiBody } from '@nestjs/swagger';
import LogInDto from './dto/logIn.dto';
import { CreateAuthorDto } from '../authors/dto/create-author.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Auth')
@Controller('authentication')
@UseInterceptors(ClassSerializerInterceptor)
export class AuthenticationController {
  constructor(
    private readonly AuthService: AuthService,
    private readonly authorsService: AuthorsService,
  ) {}

  @Post('register')
  async register(@Body() registrationData: CreateAuthorDto) {
    const author = await this.AuthService.register(registrationData);
    return author;
  }

  @HttpCode(200)
  @UseGuards(LocalAuthenticationGuard)
  @Post('log-in')
  @ApiBody({ type: LogInDto })
  async logIn(@Req() request: RequestWithUser) {
    const { author } = request;
    const accessTokenCookie = this.AuthService.getCookieWithJwtAccessToken(
      author.id,
      author.fullName,
    );
    const { cookie: refreshTokenCookie, token: refreshToken } =
      this.AuthService.getCookieWithJwtRefreshToken(author.id, author.fullName);

    await this.authorsService.setCurrentRefreshToken(refreshToken, author.id);

    request.res.setHeader('Set-Cookie', [
      accessTokenCookie,
      refreshTokenCookie,
    ]);

    return author;
  }

  @UseGuards(JwtAuthenticationGuard)
  @Post('log-out')
  @HttpCode(200)
  async logOut(@Req() request: RequestWithUser) {
    await this.authorsService.removeRefreshToken(request.author.id);
    request.res.setHeader('Set-Cookie', this.AuthService.getCookiesForLogOut());
  }

  @UseGuards(JwtAuthenticationGuard)
  @Get()
  authenticate(@Req() request: RequestWithUser) {
    return request.author;
  }

  @UseGuards(JwtRefreshGuard)
  @Get('refresh')
  refresh(@Req() request: RequestWithUser) {
    const accessTokenCookie = this.AuthService.getCookieWithJwtAccessToken(
      request.author.id,
      request.author.fullName,
    );

    request.res.setHeader('Set-Cookie', accessTokenCookie);
    return request.author;
  }
}
